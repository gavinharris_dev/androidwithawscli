# Get base image
FROM mingc/android-build-box:latest

# Install Node and NPM and AppCenter CLI
RUN apt install nodejs && apt install npm && npm install -g appcenter-cli

ENV DEBIAN_FRONTEND noninteractive
# Install the AWS SDK and zipalign
RUN apt-get update -yqq && apt-get install -y awscli zipalign && mkdir -p /usr/local/bin/ && ln -s /usr/bin/aws /usr/local/bin/
